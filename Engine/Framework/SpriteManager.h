﻿#ifndef __SPRITEMANAGER_H__
#define __SPRITEMANAGER_H__

#include <map> // map contain a pair data with key and value
// http://www.cplusplus.com/reference/map/map
#include <fstream>

#include "define.h"
#include "Sprite.h"

class SpriteManager
{
public:
	static SpriteManager* getInstance();
	static void release();

	map<eID, Sprite*>* getListSprite();
	Sprite* getSprite(eID id);
	RECT getSourceRect(eID id, string name);
	GVector2 getOrigin(eID id, string name);

	void loadSpriteInfo(eID id, const char* fileInfoPath);
	Sprite* loadXMLDoc(LPD3DXSPRITE spritehandle, LPWSTR path);
	void loadXML(eID id, LPWSTR XMLPath);

	void releaseSprite(eID id);
	void releaseTexture(eID id);

	~SpriteManager(void);
private:
	SpriteManager(void);
	static SpriteManager* s_instance;
	map<eID, Sprite*> _listSprite;
	map<eID, map<string, RECT>> _sourceRectList;
	map<eID, map<string, GVector2>> _originList;
};


#endif // !__SPRITEMANAGER_H__
