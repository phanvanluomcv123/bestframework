#include "Jumping.h"
#include "Falling.h"
#include "trace.h"

#define MAX_HEIGHT		150.0f
#define VELOCITY_X		250.f
#define ACCELERATE_X	78.48f
#define VELOCITY_Y		250.f
#define ACCELERATE_Y	78.48f

Jumping::Jumping()
{
	_movement->setAccely(ACCELERATE_Y);
	_movement->setVy(VELOCITY_Y);
}

Jumping::~Jumping()
{
}

eStatus Jumping::getState()
{
	return eStatus::JUMP;
}

void Jumping::update(float deltaTime)
{
	if (_player->getPosition().y >= MAX_HEIGHT)
	{
		// Falling
		this->setState(new Falling);
	}
}

void Jumping::updateInput(float deltaTime)
{
	if (InputController::getInstance()->isKeyDown(DIK_LEFTARROW))
	{
		_player->setFlipX(true);
		_player->setMoveDirection(eMoveDirection::MOVE_LEFT);
		_movement->setAccelx(-ACCELERATE_X);
		_movement->setVx(-VELOCITY_X);
	}
	else if (InputController::getInstance()->isKeyDown(DIK_RIGHTARROW))
	{
		_player->setFlipX(false);
		_player->setMoveDirection(eMoveDirection::MOVE_RIGHT);
		_movement->setAccelx(ACCELERATE_X);
		_movement->setVx(VELOCITY_X);
	}

	_movement->setAccely(ACCELERATE_Y);
	_movement->setVy(VELOCITY_Y);
}