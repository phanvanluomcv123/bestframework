#include "Falling.h"
#include "Standing.h"

#define GRAVITY			-9.81f
#define VELOCITY_X		200.f
#define ACCELERATE_X	9.81f
#define VELOCITY_Y		-250.f


Falling::Falling()
{
	_gravity->setStatus(eGravityStatus::FALLING__DOWN);
	_gravity->setgy(GRAVITY);
}

Falling::~Falling()
{
}

eStatus Falling::getState()
{
	return eStatus::FALL;
}

void Falling::update(float deltaTime)
{
	if (_player->getPosition().y <= 50)
	{
		_gravity->setStatus(eGravityStatus::SHALLOWED);
		this->setState(new Standing);
	}
}

void Falling::updateInput(float deltaTime)
{
	if (InputController::getInstance()->isKeyDown(DIK_LEFTARROW))
	{
		_player->setFlipX(true);
		_player->setMoveDirection(eMoveDirection::MOVE_LEFT);
		_movement->setAccelx(-ACCELERATE_X);
		_movement->setVx(-VELOCITY_X);
	}
	else if (InputController::getInstance()->isKeyDown(DIK_RIGHTARROW))
	{
		_player->setFlipX(false);
		_player->setMoveDirection(eMoveDirection::MOVE_RIGHT);
		_movement->setAccelx(ACCELERATE_X);
		_movement->setVx(VELOCITY_X);
	}

	_movement->setVy(VELOCITY_Y);
}