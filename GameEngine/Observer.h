#pragma once

class KeyEventArg;
class IObserver
{
public:
	virtual void eventKeyDown(KeyEventArg* e);
};

class IObserverKeyUp : public IObserver
{
public:
	virtual void eventKeyUp(KeyEventArg* e);
};