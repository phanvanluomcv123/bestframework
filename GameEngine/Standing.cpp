#include "Standing.h"
#include "Running.h"

Standing::Standing()
{
	_movement->setVelocity(VECTOR2ZERO);
	_movement->setAccelerate(VECTOR2ZERO);
}

Standing* Standing::create()
{
	return new Standing;
}

void Standing::update(float deltaTime)
{
	// Do nothing
}

void Standing::updateInput(float deltaTime)
{
	_movement->setVelocity(VECTOR2ZERO);
	_movement->setAccelerate(VECTOR2ZERO);

	if (InputController::getInstance()->isKeyDown(DIK_RIGHTARROW) ||
		InputController::getInstance()->isKeyDown(DIK_LEFTARROW))
	{
		setState(new Running);
	}
}

eStatus Standing::getState()
{
	return eStatus::STAND;
}
