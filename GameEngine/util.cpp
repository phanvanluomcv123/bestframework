
#include "util.h"

#include "../Engine/Framework/Event.h"
#include "../Engine/Framework/SpriteManager.h"
#include "../Engine/Framework/StageManager.h"

void Utils::loadResource(ID3DXSprite* sprite)
{
	SpriteManager* spriteManager = SpriteManager::getInstance();

	Sprite* spr = new Sprite(sprite, L"Resource/xman_sprite.png");
	spriteManager->getListSprite()->insert(make_pair(eID::XMAN, spr));
	spriteManager->loadXML(eID::XMAN, L"Resource/xman_spritesheet.xml");

	spr = new Sprite(sprite, L"Resource/MAP_1.png");
	spr = spriteManager->loadXMLDoc(sprite, L"Resource/MAP_1.tmx");
	spr->setOrigin(VECTOR2ZERO);
	spriteManager->getListSprite()->insert(make_pair(eID::MAP_STAGE_MEGAMAN, spr));

}

void Utils::loadSound(HWND hWnd)
{
	// Load sound here
}

void Utils::loadStage()
{
	StageManager* stage = StageManager::getInstance();
	stage->getListStage()->insert(make_pair(eID::MAP_STAGE_MEGAMAN, "Resource/MAP_1.tmx"));
}