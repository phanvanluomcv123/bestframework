#pragma once
#include "../../Engine/Framework/InputController.h"
#include "../../Engine/Framework/Sprite.h"
#include "../../Engine/Framework/Animation.h"
#include "../Observer.h"

class PlayerState;

enum class eMoveDirection
{
	NONE,
	MOVE_LEFT,
	MOVE_RIGHT
};

class CPlayer : public IObserverKeyUp
{
public:
	CPlayer();
	~CPlayer();

	void update(float deltaTime);
	void updateInput(float deltaTime);
	void draw(ID3DXSprite* spriteHandler, Viewport* viewport);
	void setState(PlayerState* newState);
	void setState(eStatus status);

	 void eventKeyUp(KeyEventArg* e) override;
	 void eventKeyDown(KeyEventArg* e) override;

private:
	Sprite*						_sprite;
	map<eStatus, Animation*>	_spriteAnimation;
	eStatus						_currentIndexState;
	map<string, IComponent*>	_component;
	InputController*			_input;

	PlayerState*				_playerState;

	eStatus						_status;

	bool						_isFlipX;

	eMoveDirection				_MoveDirection;

	bool						_isJumping;

	bool						_allowShoot;

	float						_timeShoot;

public:
	void setPosition(GVector2 position);
	GVector2 getPosition();

	void setScale(float scale);

	IComponent* getComponent(string name);

	GVector2 getVelocity();

	void setFlipX(bool isFlip);

	void setMoveDirection(eMoveDirection moveDirection);
	eMoveDirection getMoveDirection();
};