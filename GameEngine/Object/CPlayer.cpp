#include "CPlayer.h"
#include "../PlayerState.h"
#include "../Standing.h"
#include "../Jumping.h"
#include "../Falling.h"
#include "../trace.h"
#include "../Dashing.h"

CPlayer::CPlayer()
{
	_input = InputController::getInstance();
	_input->Attach(this);

	_sprite = SpriteManager::getInstance()->getSprite(eID::XMAN);
	_sprite->setFrameRect(SpriteManager::getInstance()->getSourceRect(eID::XMAN, "stand_1"));
	_sprite->setZIndex(1.0f);

	Movement* movement = new Movement(VECTOR2ZERO, VECTOR2ZERO, _sprite);
	_component.insert(make_pair("Movement", movement));

	Gravity* gravity = new Gravity(VECTOR2ZERO, movement);
	_component.insert(make_pair("Gravity", gravity));

	_spriteAnimation[eStatus::STAND] = new Animation(_sprite, 0.15f);
	_spriteAnimation[eStatus::STAND]->addFrameRect(eID::XMAN, "standing_1", "standing_2", NULL);

	_spriteAnimation[eStatus::STAND_SHOOT] = new Animation(_sprite, 0.15f);
	_spriteAnimation[eStatus::STAND_SHOOT]->addFrameRect(eID::XMAN, "StandingShoot_1", "StandingShoot_2", NULL);


	_spriteAnimation[eStatus::RUN] = new Animation(_sprite, 0.05f);
	_spriteAnimation[eStatus::RUN]->addFrameRect(eID::XMAN, "StartRunning_1", "StartRunning_2",
	                                             "running_1", "running_2", "running_3", "running_4", "running_5",
	                                             "running_6", "running_7", "running_8", "running_9", NULL);
	_spriteAnimation[eStatus::RUN]->animateFromTo(3, 10);

	_spriteAnimation[eStatus::RUN_SHOOT] = new Animation(_sprite, 0.05f);
	_spriteAnimation[eStatus::RUN_SHOOT]->addFrameRect(eID::XMAN,
	                                                   "RunningShoot_1", "RunningShoot_2", "RunningShoot_3",
	                                                   "RunningShoot_4", "RunningShoot_5", "RunningShoot_6",
	                                                   "RunningShoot_7", "RunningShoot_8", "RunningShoot_9", NULL);


	_spriteAnimation[eStatus::JUMP] = new Animation(_sprite, 0.1f);
	_spriteAnimation[eStatus::JUMP]->setLoop(false);
	_spriteAnimation[eStatus::JUMP]->addFrameRect(eID::XMAN, "jumping_1", "jumping_2", "jumping_3", NULL);

	_spriteAnimation[eStatus::JUMP_SHOOT] = new Animation(_sprite, 0.1f);
	_spriteAnimation[eStatus::JUMP_SHOOT]->setLoop(false);
	_spriteAnimation[eStatus::JUMP_SHOOT]->addFrameRect(eID::XMAN, "JumpingShoot_1", "JumpingShoot_2", "JumpingShoot_3",
	                                                    NULL);


	_spriteAnimation[eStatus::FALL] = new Animation(_sprite, 0.1f);
	_spriteAnimation[eStatus::FALL]->setLoop(false);
	_spriteAnimation[eStatus::FALL]->addFrameRect(eID::XMAN, "falling_1", "falling_2", "falling_3", "falling_4", NULL);

	_spriteAnimation[eStatus::FALL_SHOOT] = new Animation(_sprite, 0.1f);
	_spriteAnimation[eStatus::FALL_SHOOT]->setLoop(false);
	_spriteAnimation[eStatus::FALL_SHOOT]->addFrameRect(eID::XMAN, "FallingShoot_1", "FallingShoot_2", "FallingShoot_3",
	                                                    "FallingShoot_4", NULL);

	_spriteAnimation[eStatus::DASH] = new Animation(_sprite, 0.1f);
	_spriteAnimation[eStatus::DASH]->setLoop(false);
	_spriteAnimation[eStatus::DASH]->addFrameRect(eID::XMAN, "dashing_1", "dashing_2", NULL);

	_spriteAnimation[eStatus::DASH_SHOOT] = new Animation(_sprite, 0.1f);
	_spriteAnimation[eStatus::DASH_SHOOT]->setLoop(false);
	_spriteAnimation[eStatus::DASH_SHOOT]->addFrameRect(eID::XMAN, "DashShoot_1", "DashShoot_2", NULL);

	// Default current index of state is standing
	_currentIndexState = eStatus::STAND;

	// Setting status for player
	PlayerState::setPlayer(this);
	setState(new Standing);

	// Don't reverse
	_isFlipX = false;

	// Move direction default is not moving
	_MoveDirection = eMoveDirection::NONE;

	// Not jumping
	_isJumping = false;

	// Allow to shoot
	_allowShoot = true;

	// Time shoot
	_timeShoot = 0.f;
}

CPlayer::~CPlayer()
{
	/* Delete sprite */
	SAFE_DELETE(_sprite);

	/* Delete sprite animation */
	for (auto & it : _spriteAnimation)
	{
		delete it.second;
	}

	/* Delete component */
	for (auto & it : _component)
	{
		delete it.second;
	}

	/* Delete the state of player */
	SAFE_DELETE(_playerState);

	_input->Detach(this);
}

void CPlayer::update(float deltaTime)
{
	/* Update component */
	for (auto & it : _component)
	{
		it.second->update(deltaTime);
	}

	/* Update sprite animation */
	_spriteAnimation[_currentIndexState]->update(deltaTime);

	/* Update player state */
	_playerState->update(deltaTime);
}

void CPlayer::updateInput(float deltaTime)
{
	_timeShoot += deltaTime / 1000.f;
	if ((_timeShoot > 1.f) && !_allowShoot)
	{
		_allowShoot = true;
		_timeShoot = 0.f;
		_status = eStatus(static_cast<int>(_currentIndexState) & ~static_cast<int>(eStatus::SHOOT));
		this->setState(_status);
	}
	// Update input for status of player
	_playerState->updateInput(deltaTime);
}

void CPlayer::draw(ID3DXSprite* spriteHandler, Viewport* viewport)
{
	// Flip sprite to coordinate-x
	_sprite->setFlipX(_isFlipX);
	// Draw sprite animation
	_spriteAnimation[_currentIndexState]->draw(spriteHandler, viewport);
}

void CPlayer::setState(PlayerState* newState)
{
	// Delete previous state
	SAFE_DELETE(_playerState);
	// Set new state
	_playerState = newState;
	// Set current index of state for new index of state
	_currentIndexState = newState->getState();
	// Restart animation
	_spriteAnimation[_currentIndexState]->restart();
}

void CPlayer::setState(eStatus status)
{
	// Get index of previous state
	auto index = _spriteAnimation[_currentIndexState]->getIndex();
	// Get time animation of previous state
	auto timeAnimate = _spriteAnimation[_currentIndexState]->getTimeAnimate();
	// Set current index of state for new index of state
	_currentIndexState = status;
	// Set time animate
	_spriteAnimation[status]->setTimeAnimate(timeAnimate);
	// Restart animation
	_spriteAnimation[status]->restart(index);
}

void CPlayer::eventKeyUp(KeyEventArg* e)
{
	if ((_isJumping) && (e->_key == DIK_Z))
	{
		_isJumping = false;
		this->setState(new Falling);
	}
}

void CPlayer::eventKeyDown(KeyEventArg* e)
{
	if ((!_isJumping) && (e->_key == DIK_Z))
	{
		_isJumping = true;
		if ((_currentIndexState == eStatus::STAND) ||
			(_currentIndexState == eStatus::RUN) ||
			(_currentIndexState == eStatus::STAND_SHOOT) ||
			(_currentIndexState == eStatus::RUN_SHOOT) ||
			(_currentIndexState == eStatus::DASH) ||
			(_currentIndexState == eStatus::DASH_SHOOT))
		{
			this->setState(new Jumping);
		}
	}

	if ((e->_key == DIK_C))
	{
		if ((_currentIndexState == eStatus::STAND) ||
			(_currentIndexState == eStatus::RUN) ||
			(_currentIndexState == eStatus::STAND_SHOOT) ||
			(_currentIndexState == eStatus::RUN_SHOOT))
		{
			this->setState(new Dashing);
		}
	}

	if ((e->_key == DIK_X) && (_allowShoot))
	{
		_timeShoot = 0.f;
		_allowShoot = false;
		_status = eStatus(static_cast<int>(_currentIndexState) | static_cast<int>(eStatus::SHOOT));
		this->setState(_status);
	}
}

void CPlayer::setPosition(GVector2 position)
{
	_sprite->setPosition(position);
}

GVector2 CPlayer::getPosition()
{
	return _sprite->getPosition();
}

void CPlayer::setScale(float scale)
{
	_sprite->setScale(scale);
}

IComponent* CPlayer::getComponent(string name)
{
	return _component[name];
}

GVector2 CPlayer::getVelocity()
{
	Movement* movement = static_cast<Movement*>(_component["Movement"]);
	return movement->getVelocity();
}

void CPlayer::setFlipX(bool isFlip)
{
	if (_isFlipX != isFlip)
		_isFlipX = isFlip;
}

void CPlayer::setMoveDirection(eMoveDirection moveDirection)
{
	_MoveDirection = moveDirection;
}

eMoveDirection CPlayer::getMoveDirection()
{
	return _MoveDirection;
}
