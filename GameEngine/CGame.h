#pragma once
#include <Windows.h>
#include "../Engine/Framework/Graphics.h"
#include "../Engine/Framework/DeviceManager.h"
#include "../Engine/Framework/GameTime.h"
#include "../Engine/Framework/InputController.h"

class CGame
{
public:
	virtual ~CGame() = default;
	explicit CGame(HINSTANCE hInstance, LPWSTR = L"Window Game", int width = 800, int height = 600, int fps = 60, int isFullScreen = 0);
	virtual void init();
	virtual void release();

	virtual void updateInput(float deltaTime);
	virtual void update(float deltaTime);
	virtual void draw();

	virtual void run();
	virtual void render();

	virtual void loadResource();

	static void exit();

	static Graphics* getWindows();

	static LRESULT CALLBACK wWinProc(HWND wnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

protected:
	DeviceManager*		_pDeviceManager;
	GameTime*			_pGameTime;
	ID3DXSprite*		_D3DXSprite;
	InputController*	_pInput;
	static Graphics*	s_hWindows;

	static bool			s_bIsExited;

	float				_deltaTime;
	float				_oldTime;
	float				_frameRate;
};