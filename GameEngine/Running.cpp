#include "Running.h"
#include "Standing.h"

#define MAX_VELOCITY_LEFT	-350.0f
#define MAX_VELOCITY_RIGHT	350.0f
#define VELOCITY_X			250.f
#define ACCELERATE_X		9.81f

Running::Running()
{
}

void Running::update(float deltaTime)
{
	if (_player->getMoveDirection() == eMoveDirection::MOVE_LEFT)
	{
		if (_movement->getVelocity().x <= MAX_VELOCITY_LEFT)
			_movement->setVx(MAX_VELOCITY_LEFT);
	}
	else if (_player->getMoveDirection() == eMoveDirection::MOVE_RIGHT)
	{
		if (_movement->getVelocity().x >= MAX_VELOCITY_RIGHT)
			_movement->setVx(MAX_VELOCITY_RIGHT);
	}
}

void Running::updateInput(float deltaTime)
{
	if (InputController::getInstance()->isKeyDown(DIK_LEFTARROW))
	{
		_player->setFlipX(true);
		_player->setMoveDirection(eMoveDirection::MOVE_LEFT);
		_movement->setVx(-VELOCITY_X);
		_movement->setAccelx(-ACCELERATE_X);
	}
	else if (InputController::getInstance()->isKeyDown(DIK_RIGHTARROW))
	{
		_player->setFlipX(false);
		_player->setMoveDirection(eMoveDirection::MOVE_RIGHT);
		_movement->setVx(VELOCITY_X);
		_movement->setAccelx(ACCELERATE_X);
	}
	else
	{
		_player->setMoveDirection(eMoveDirection::NONE);
		this->setState(new Standing);
	}
}

eStatus Running::getState()
{
	return eStatus::RUN;
}
