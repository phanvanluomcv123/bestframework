#include "Dashing.h"
#include "Jumping.h"
#include "Standing.h"
#include "trace.h"

eStatus Dashing::getState()
{
	return eStatus::DASH;
}

void Dashing::update(float deltaTime)
{
}

void Dashing::updateInput(float deltaTime)
{
	if (_timeDash > 0.7f)
	{
		_timeDash = 0.f;
		this->setState(new Standing);
	}
}
