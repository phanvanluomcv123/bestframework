#include "PlayScene.h"
#include "../Engine/Framework/StageManager.h"

PlayScene::PlayScene() : _player(nullptr), _tileMap(nullptr)
{
}

PlayScene::~PlayScene()
{
}

bool PlayScene::init()
{
	_viewport = new Viewport(0, WINDOW_HEIGHT, WINDOW_WIDTH, WINDOW_HEIGHT);
	_player = new CPlayer;
	_player->setPosition(GVector2(100.f, 50.f));
	_player->setScale(2);
	_tileMap = StageManager::getInstance()->getTileMap(eID::MAP_STAGE_MEGAMAN);
	return true;
}

void PlayScene::updateInput(float dt)
{
	// Update input for player
	_player->updateInput(dt);
}

void PlayScene::update(float dt)
{
	// Update viewport
	updateViewport(dt);

	// Update player
	_player->update(dt);
}

void PlayScene::draw(LPD3DXSPRITE spriteHandle)
{
	// Draw tile map
	_tileMap->draw(spriteHandle, _viewport);

	// Draw player
	_player->draw(spriteHandle, _viewport);
}

void PlayScene::release()
{
	SAFE_DELETE(_viewport);
	SAFE_DELETE(_player);
	_tileMap->release();
	SAFE_DELETE(_tileMap);
}

void PlayScene::updateViewport(float dt)
{
	GVector2 currentPosition = _viewport->getPositionWorld();
	GVector2 worldSize = _tileMap->getWorldSize();
	auto playerX = _player->getPosition().x;

	GVector2 new_position = GVector2(max(playerX - WINDOW_WIDTH / 2, 0), WINDOW_HEIGHT);

	if (_player->getVelocity().x == 0)
	{
		new_position = currentPosition;
	}

	if (new_position.x + WINDOW_WIDTH > worldSize.x)
	{
		new_position.x = worldSize.x - WINDOW_WIDTH;
	}

	_viewport->setPositionWorld(new_position);
}