#include "CMegaman.h"
#include "util.h"
#include "../Engine/Framework/SceneManager.h"

CMegaman::CMegaman(HINSTANCE hInstance, LPWSTR title) : CGame(hInstance, title, WINDOW_WIDTH, WINDOW_HEIGHT)
{
}

CMegaman::~CMegaman()
{
}

void CMegaman::init()
{
	CGame::init();
	SceneManager::getInstance()->addScene(new PlayScene);
}

void CMegaman::release()
{
	CGame::release();
	SceneManager::getInstance()->getCurrentScene()->release();
	SceneManager::getInstance()->clearScenes();
}

void CMegaman::update(float deltaTime)
{
	// Update player
	SceneManager::getInstance()->getCurrentScene()->update(deltaTime);
}

void CMegaman::draw()
{
	// Start drawing
	_D3DXSprite->Begin(D3DXSPRITE_ALPHABLEND);

	// Draw scene
	SceneManager::getInstance()->getCurrentScene()->draw(_D3DXSprite);

	// End drawing
	_D3DXSprite->End();
}

void CMegaman::loadResource()
{
	// Load resource here
	Utils::loadResource(_D3DXSprite);
	Utils::loadResource(_D3DXSprite);
	Utils::loadStage();
	Utils::loadSound(s_hWindows->getWnd());
}

void CMegaman::updateInput(float deltaTime)
{
	// Update input for current scene
	SceneManager::getInstance()->getCurrentScene()->updateInput(deltaTime);
}
