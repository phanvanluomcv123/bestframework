#pragma once
#include "../Engine/Framework/Sprite.h"
#include "../Engine/Framework/dxaudio.h"

class EventArg;

class Utils
{
public:
	static void loadResource(ID3DXSprite* sprite);
	static void loadSound(HWND hWnd);
	static void loadStage();
};

