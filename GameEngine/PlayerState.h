#pragma once
#include "Object/CPlayer.h"

class PlayerState
{
public:
	PlayerState();
	~PlayerState();

	virtual void update(float deltaTime);
	virtual void updateInput(float deltaTime);
	virtual eStatus getState() = 0;

protected:
	static CPlayer* _player;
	Movement*		_movement;
	Gravity*		_gravity;

public:
	void setState(PlayerState* newState);
	static void setPlayer(CPlayer* player);
};